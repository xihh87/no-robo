## Descripción del proyecto

<!-- 5 cuartillas máximo, 3 de diciembre -->

Hay varias cosas que se pueden hacer para reducir el robo de identidad:

  - Evitar que funcione.

    Requeríría educar a la población o establecer mecanismos para hacer más difícil solicitar los créditos.
    Este método es costoso y sería más efectivo si tuviéramos identificada a la población más vulnerable.

  - Facilitar que se encuentre a los responsables.

    Una de las razones por las que este delito tiene éxito es que no se puede perseguir hasta que algún daño ha ocurrido. Sin embargo, puede facilitarse el proceso de denuncia para tener más datos para identificarlos, mejorar los incentivos para perseguirlos mejorar la medición de la incidencia real de este delito.

    **Este proyecto pretende mejorar la usabilidad del MP virtual para facilitar al usuario hacer su denuncia en una forma que proceda y se pueda investigar**.

    El proceso de investigación, sin embargo, requiere que se promueva el caso. Esto tendría que financiarse entre los afectados (víctimas y aseguradoras), que sin embargo tienen intereses encontrados porque la aseguradora pretende pagar lo menos posible para tener ganancias y la víctima pretende no pagar al banco. Ambos, sin embargo, se benefician de que disminuya la incidencia de este tipo de delitos porque implicaría ahorros para la aseguradora y un riesgo menor para la víctima. Estas acciones por el momento están fuera del alcance del proyecto.

  - Mejorar la medición de la incidencia del delito.

    La incidencia de este tipo de delito es difícil de medir porque quienes se dan cuenta del acto no continúan el proceso. Es decir, el delito únicamente ocurre cuando las personas no identifican que sucede. Las víctimas generalmente se dan cuenta del problema cuando ya hubo afectación a su patrimonio y es hasta entonces que pueden denunciarlo.

    Sospecho que la mayoría de los esfuerzos de las víctimas se enfocan en comprobar que ellos no fueron los solicitantes del crédito, o realizaron la transferencia, o son los responsables del acto en que se afectó su patrimonio. La mayoría no tiene idea acerca de cómo ocurrió y no pueden dar muchos detalles que ayuden a la investigación.

    Existen varios servicios en donde se pueden reportar los números utilizados por los criminales, pero esta aproximación tiene las siguientes limitantes:

    - La información siempre es limitada porque los criminales pueden comprar otras líneas por menos de MXN $100 cada una, además de que seguramente dejan de usar los números después de un tiempo.
    - La información está fragmentada en los diferentes servicios que intentan ayudar, y esos servicios no se hablan entre sí.
    - Algunos de estos servicios son difíciles de consultar y no pueden asociarse a la identificación de llamadas del celular.
    - Es difícil usar la mayoría de estos servicios.
    - Los servicios no tienen forma de verificar la información y dependen de los reportes de usuarios.

## Recursos

<!-- 5 cuartillas máximo, 3 de diciembre -->
