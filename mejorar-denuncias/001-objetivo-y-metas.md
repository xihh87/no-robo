\newpage

## Objetivo y metas

<!-- 2 cuartillas máximo, 19 de noviembre -->

Mejorar la usabilidad del ministerio público virtual para:

  - Disminuir el número de reportes erróneos

  - Aumentar el número de reportes correctos

  - Permitir la retroalimentación acerca de los incidentes que se reportan

  - Aumentar la productividad del personal que revisa las solicitudes
