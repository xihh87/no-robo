---
title: "¿Qué hacer para evitar un robo de identidad en México?"
author: Joshua Haase
date: 2019-09-29
---

En 2017 Alf Göransson, CEO de Securitas,
[fue víctima de robo de identidad](https://www.bbc.com/mundo/noticias-internacional-40594807).
Solicitaron un crédito en su nombre y lo declararon en bancarrota.

La historia tuvo un final feliz,
pero es un ejemplo de que incluso personas educadas e inteligentes
pueden ser víctimas de este delito.

Para suplantar a alguien sólo se necesita:

  - Nombre
  - Dirección
  - Fecha de Nacimiento

Así que si alguien pide los datos por teléfono o en internet,
eso debería ser una señal de problemas.

Si alguien quiere venderte un servicio legítimo generalmente pedirá una cita para reunirse contigo,
y no pedirá estos datos sino que los obtendrá de la documentación oficial necesaria para el trámite.

# ¿Qué pueden hacer con mis datos?

- Solicitar créditos en tu nombre

- Obtener documentos

- Modificar tus datos en alguna institución

- Asociarte a algún delito

- cometer otro delito,

- contratar servicios de telefonía,

- obtener una hipoteca o un crédito,

- realizar compras tanto en tiendas físicas como a través de tiendas online, etc.

# ¿Qué hacer si di mis datos?

Lo primero es guardar registro de llamadas y transacciones en línea.

Lo siguiente es levantar un [acta especial de hechos](https://mpvirtual.pgj.cdmx.gob.mx/CiberDenuncia/TerminosCondiciones.aspx).
(Porque el robo de identidad no constituye un delito y por lo tanto no puede investigarse hasta que se ha causado daño.)

Después de hacer la declaración virtual, es necesario ir a una oficina del MP para hacer oficial el documento.
Hay que guardar este documento y anotar el número de la identificación.

Para prevenir que usen tus datos para solicitar créditos
puedes [bloquear el acceso al buró de crédito](https://wbc1.burodecredito.com.mx:7442/idprovider/pages/datosGrales.jsf?gatm=15).

Puedes reportar el número en el [Portal de Fraudes Financieros de la CONDUSEF](https://phpapps.condusef.gob.mx/fraudes_financieros/monitor.php)
por si a alguien se le ocurre buscar el número.


# ¿Qué acciones legales se pueden tomar?

La usurpación de identidad es un delito de acuerdo al artículo 211 bis del [Código Penal Federal](http://www.diputados.gob.mx/LeyesBiblio/pdf/9_120419.pdf)
y [CONDUSEF estima daño económico de más de MXN $260 millones anuales](https://www.eleconomista.com.mx/sectorfinanciero/En-tres-semanas-150-robos-de-identidad-detectados-20160311-0014.html).



- - -

[Ley Federal de Protección de Datos Personales en Posesión de Particulares](http://www.diputados.gob.mx/LeyesBiblio/pdf/LFPDPPP.pdf)

- - -

[Ejemplo en Europa](https://www.internautas.org/html/9347.html)

[Robo de identidad](https://www.consumidor.gov/articulos/s1015-evitar-el-robo-de-identidad#!qu%C3%A9-hacer)

[Una narración que podría mejorarse acerca de cómo roban la identidad](https://www.dineroenimagen.com/tu-dinero/robaron-mi-identidad-pidieron-credito-mi-nombre-y-ahora-vivo-una-pesadilla/96648#view-1)

[Qué hacer en Europa](https://ayudaleyprotecciondatos.es/2018/09/10/suplantacion-identidad/)

[¿Qué datos dar y qué datos no dar por teléfono?](https://www.excelsior.com.mx/blog/el-guru-de-tus-finanzas/que-datos-dar-y-no-por-telefono-sobre-tu-tarjeta-de-credito/1221217)

[Recomendaciones de la CONDUSEF para prevenir el robo de identidad](https://www.condusef.gob.mx/Revista/index.php/usuario-inteligente/consejos-de-seguridad/497-tus-datos-personales-valen-oro-protegelos)

[La identificación de llamadas no necesariamente es confiable](https://www.condusef.gob.mx/Revista/index.php/usuario-inteligente/consejos-de-seguridad/967-que-es-eso)
