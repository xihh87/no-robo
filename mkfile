MKSHELL=mkx

out/manuscript.md:Q:	presentación
	mkvars
	manubot
		process
		--content-directory ${prereq}
		--output-directory out

out/presentación.pdf:Q:	out/manuscript.md
	mkvars
	pandoc
		-f markdown+grid_tables+table_captions+pipe_tables+example_lists+fenced_code_attributes+fenced_divs+bracketed_spans+footnotes
		-t beamer
		--filter pandoc-citeproc
		--bibliography out/references.json
		-o ${target}
		${prereq}

out/presentación.html:Q:	out/manuscript.md
	mkvars
	pandoc
		-f markdown+grid_tables+table_captions+pipe_tables+example_lists+fenced_code_attributes+fenced_divs+bracketed_spans+footnotes
		-t revealjs
		--filter pandoc-citeproc
		--standalone
		--self-contained
		--bibliography out/references.json
		-o ${target}
		${prereq}

work/manuscript.md:Q:	mejorar-denuncias
	mkvars
	manubot
		process
		--content-directory ${prereq}
		--output-directory work

work/entrega.pdf:Q:	work/manuscript.md
	mkvars
	pandoc
		-f markdown+grid_tables+table_captions+pipe_tables+example_lists+fenced_code_attributes+fenced_divs+bracketed_spans+footnotes
                --pdf-engine=xelatex
                -V documentclass=scrartcl
                -V logo=ucha.png
                --data-dir=./pandoc/
                --template ucha
                --filter=pandoc-citeproc
		--bibliography work/references.json
		-o ${target}
		${prereq}

clean:Q:
	rm -rf out work
