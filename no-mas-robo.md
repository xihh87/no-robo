Ella no tenía medio millón de pesos y no podía creer que se los hubieran robado.

Ella habría pensado que eso no le pasaría porque obvio no tenía medio millón de pesos,
pero ha pasado los últimos tres años peleando y ha gastado decenas de miles de pesos
para no tener que pagarlos nuevamente.

El problema no es sólo de mi amiga o su mamá,
y le cuesta a México directamente [$260 millones de pesos cada año](https://www.eleconomista.com.mx/sectorfinanciero/En-tres-semanas-150-robos-de-identidad-detectados-20160311-0014.html).

Pero el costo real es más que eso, porque nuestra sociedad se basa en confianza y la confianza es uno de los recursos más valiosos de un país.

Cuando tienes la confianza de que comprar en internet, puedes pedir en dos minutos una televisión desde la comodidad de un sillón y tomando café.
Cuando tienes miedo de comprar en internet tienes que tomar media hora de transporte, 30 minutos decidiendo en una prisión abarrotada de gente, tienes menos opciones. Si confías en las personas la podrían llevar ellos a tu casa. Si no, tienes que resolver ese problema además.

Si tomamos en cuenta las pérdidas indirectas, sería mucho más de $260 millones.
Todo el gasto en seguridad es un costo indirecto de la falta de confianza en una sociedad.

> La seguridad es el precio que pagamos porque no somos ángeles.

Parecería que hablo de un problema lejano, de algunas personas pero no de nosotros.
Es algo que no le pasa a personas sofisticadas como el CEO empresas de seguridad porque están protegidas,
saben lo que hacen y tienen mucho cuidado.
[Pero le robaron la identidad al CEO de Securitas en 2017](https://www.bbc.com/mundo/noticias-internacional-40594807).

Incluso a personas sofisticadas, inteligentes y que tienen cuidado les ha pasado. No hay razón para pensar que «no me va a pasar a mi».

## ¿Deberíamos preocuparnos?

Bruce Schneier dice que si lo ves en las noticias no debes preocuparte porque es algo que es lo suficientemente raro para ser notable.
Pero si te lo cuenta un amigo, entonces sí debes preocuparte, porque es algo que pasa mucho y cerca de tí.

Y yo sé que está cerca y pasa mucho porque mis amigos me han contado de este problema.

## ¿Por qué la gente roba?

En cierta forma es una pregunta muy incorrecta.
La verdadera pregunta no es por qué lo hacen, sino por qué las personas generalmente no aprovechan cada ocasión de obtener un beneficio inmediato.

  - ¿El castigo es mayor que el beneficio?

  - ¿La probabilidad de ser atrapado?

  - ¿Qué imagen nos deja en nuestro círculo social?

  - ¿Qué tan fácil es hacer las cosas mal?

  - ¿Qué tan fácil es hacer las cosas bien?

  - ¿Qué tanto beneficio deja hacer las cosas bien?

  - ¿Qué tanto beneficio deja hacer las cosas mal?
